using Movement;
using UnityEngine;

namespace CharacterBehaviours
{
    public class Player: Character, ICharacter
    {
        [SerializeField] private Pointer Pointer;

        private void FixedUpdate()
        {
            _characterView.Walk(!Pointer.isCalm);
            if(!Pointer.isCalm) GoToPoint(Pointer.transform.position);
        }
    }
} 